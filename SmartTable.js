/**
 * Component with ability to sort column by clicking on corresponding header
 * @param {HTMLElement} parentElement
 * @param {Array.<Object>} array
 * @constructor
 */
function SmartTable(parentElement, array) {

}

/**
 * Sort table by 'column' in ascending order if 'asc' == true, in descending order otherwise
 * @param {string} column
 * @param {boolean} asc
 */
SmartTable.prototype.sort = function(column, asc) {

};