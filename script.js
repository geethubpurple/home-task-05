window.addEventListener('load', function() {

    function createElement(tagName, parentElement, className) {
        var element = parentElement.ownerDocument.createElement(tagName);
        parentElement.appendChild(element);
        if (typeof(className) == 'string') {
            element.className = className;
        }
        return element;
    }

    var div, h3;

    // Students
    div = createElement('div', document.body);
    h3 = createElement('h3', div);
    h3.textContent = 'Students';
    var tableStudents = new SmartTable(div, students);
    tableStudents.sort('email', true);

    // Pictures
    div = createElement('div', document.body);
    h3 = createElement('h3', div);
    h3.textContent = 'Pictures';
    var tablePictures = new SmartTable(div, pictures);
    tablePictures.sort('mime_type', false);

    // Drugs
    div = createElement('div', document.body);
    h3 = createElement('h3', div);
    h3.textContent = 'Drugs';
    var tableDrugs = new SmartTable(div, drugs);
    tableDrugs.sort('drug_brand', true);
});