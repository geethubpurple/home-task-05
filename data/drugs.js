var drugs = [
    {
        "latitude": "46.84518",
        "longitude": "40.30834",
        "drug_company": "Nelco Laboratories, Inc.",
        "drug_brand": "Silk"
    },
    {
        "latitude": "-8.4923",
        "longitude": "123.0015",
        "drug_company": "Valeant Pharmaceuticals North America LLC",
        "drug_brand": "AcneFree"
    },
    {
        "latitude": "15.73333",
        "longitude": "-87.9",
        "drug_company": "SHISEIDO CO., LTD.",
        "drug_brand": "SHISEIDO ADVANCED HYDRO-LIQUID COMPACT (REFILL)"
    },
    {
        "latitude": "45.95056",
        "longitude": "17.22944",
        "drug_company": "CARDINAL HEALTH",
        "drug_brand": "Cold Head Congestion Daytime Non-Drowsy"
    },
    {
        "latitude": "59.3326",
        "longitude": "18.0649",
        "drug_company": "Allermed Laboratories, Inc.",
        "drug_brand": "Goat Epithelia"
    },
    {
        "latitude": "47.49989",
        "longitude": "-52.99806",
        "drug_company": "Allergy Laboratories, Inc.",
        "drug_brand": "WHITE POTATO"
    },
    {
        "latitude": "-8.514",
        "longitude": "115.1673",
        "drug_company": "American Health Packaging",
        "drug_brand": "Amlodipine Besylate"
    },
    {
        "latitude": "50.35123",
        "longitude": "36.8298",
        "drug_company": "TONYMOLY CO., LTD.",
        "drug_brand": "TONYMOLY My Sunny Perfecting Sun Block"
    },
    {
        "latitude": "26.24551",
        "longitude": "36.45248",
        "drug_company": "Uriel Pharmacy Inc.",
        "drug_brand": "Rosmarinus Lavendula Special Order"
    },
    {
        "latitude": "6.64678",
        "longitude": "-4.70519",
        "drug_company": "EnviroKleen LLC",
        "drug_brand": "Envirokleen Instant Hand Sanitizer"
    },
    {
        "latitude": "13.78005",
        "longitude": "100.54275",
        "drug_company": "Unifirst First Aid Corporation",
        "drug_brand": "Medique Pain Off"
    },
    {
        "latitude": "29.25647",
        "longitude": "111.61783",
        "drug_company": "Kmart Corporation",
        "drug_brand": "smart sense nicotine"
    },
    {
        "latitude": "20.34222",
        "longitude": "104.34464",
        "drug_company": "Apotheca Company",
        "drug_brand": "Tobaccotox"
    },
    {
        "latitude": "18.83333",
        "longitude": "-72.10528",
        "drug_company": "X3 Labs Inc.",
        "drug_brand": "X3 Clean"
    },
    {
        "latitude": "50.74159",
        "longitude": "14.31239",
        "drug_company": "REMEDYREPACK INC.",
        "drug_brand": "Acyclovir"
    },
    {
        "latitude": "27.91273",
        "longitude": "104.95047",
        "drug_company": "Chattem, Inc.",
        "drug_brand": "Icy Hot"
    },
    {
        "latitude": "39.9772",
        "longitude": "-75.2545",
        "drug_company": "Osung Co., Ltd",
        "drug_brand": "PiGe ll Real Skin Eye"
    },
    {
        "latitude": "61.67694",
        "longitude": "96.37917",
        "drug_company": "Western Family Foods Inc",
        "drug_brand": "Naproxen Sodium"
    },
    {
        "latitude": "-7.0412",
        "longitude": "113.6863",
        "drug_company": "Hangzhou Haorun Technology CO.,LTD.",
        "drug_brand": "HAND SANITIZER"
    },
    {
        "latitude": "-8.7089",
        "longitude": "120.3838",
        "drug_company": "Apotex Corp.",
        "drug_brand": "Captopril"
    },
    {
        "latitude": "52.4385",
        "longitude": "4.8264",
        "drug_company": "Wockhardt Limited",
        "drug_brand": "EXTENDED PHENYTOIN SODIUM"
    },
    {
        "latitude": "5.9765",
        "longitude": "116.1158",
        "drug_company": "Western Family Foods, Inc",
        "drug_brand": "Dandruff"
    },
    {
        "latitude": "-13.62694",
        "longitude": "-71.38528",
        "drug_company": "Lex Inc",
        "drug_brand": "Baczol Cold Medicine"
    },
    {
        "latitude": "56.89375",
        "longitude": "65.89378",
        "drug_company": "Steris Corporation",
        "drug_brand": "Hand Sanitizer"
    },
    {
        "latitude": "-28.76659",
        "longitude": "28.24936",
        "drug_company": "West-ward Pharmaceutical Corp",
        "drug_brand": "Donepezil Hydrochloride"
    },
    {
        "latitude": "53.84534",
        "longitude": "19.60519",
        "drug_company": "Kmart Corporation",
        "drug_brand": "pain relief"
    },
    {
        "latitude": "29.61904",
        "longitude": "107.66316",
        "drug_company": "ShopKo Stores",
        "drug_brand": "Anticavity"
    },
    {
        "latitude": "34.34028",
        "longitude": "134.04333",
        "drug_company": "Cardinal Health",
        "drug_brand": "PredniSONE"
    },
    {
        "latitude": "53.9162",
        "longitude": "27.2009",
        "drug_company": "Watson Laboratories, Inc.",
        "drug_brand": "Cyclobenzaprine Hydrochloride"
    },
    {
        "latitude": "13.6533",
        "longitude": "100.25972",
        "drug_company": "LEVEL ORAL CARE, LLC",
        "drug_brand": "LEVEL HERBAL MINT FIVE SENSITIVE FORMULA ANTICAVITY"
    },
    {
        "latitude": "49.93708",
        "longitude": "17.80547",
        "drug_company": "Nelco Laboratories, Inc.",
        "drug_brand": "Fremont Cottonwood"
    },
    {
        "latitude": "36.9106",
        "longitude": "121.52504",
        "drug_company": "Blossom Pharmaceuticals",
        "drug_brand": "Alcohol"
    },
    {
        "latitude": "55.71667",
        "longitude": "37.41667",
        "drug_company": "Ventura Corporation Ltd.",
        "drug_brand": "ESIKA"
    },
    {
        "latitude": "-8.269",
        "longitude": "123.4655",
        "drug_company": "FRED'S, INC.",
        "drug_brand": "Womens Gentle Laxative"
    },
    {
        "latitude": "29.7687",
        "longitude": "-95.3867",
        "drug_company": "Western Family Foods, Inc",
        "drug_brand": "Witch Hazel"
    },
    {
        "latitude": "53.29617",
        "longitude": "69.59997",
        "drug_company": "Nelco Laboratories, Inc.",
        "drug_brand": "Duck Meat"
    },
    {
        "latitude": "27.54944",
        "longitude": "109.95917",
        "drug_company": "KLE 2, Inc.",
        "drug_brand": "Tramadol Hydrochloride"
    },
    {
        "latitude": "45.4333",
        "longitude": "4.4",
        "drug_company": "Fenwal, Inc.",
        "drug_brand": "ADSOL Red Cell Preservation Solution System in Plastic Container (PL 146 Plastic)"
    },
    {
        "latitude": "-0.48193",
        "longitude": "15.89988",
        "drug_company": "H.J. Harkins Company, Inc.",
        "drug_brand": "Promethazine Hydrochloride"
    },
    {
        "latitude": "-4.90083",
        "longitude": "-80.81583",
        "drug_company": "Dr. Reddy's Laboratories Limited",
        "drug_brand": "Naproxen Tablets"
    },
    {
        "latitude": "52.34006",
        "longitude": "21.24207",
        "drug_company": "Cintex Services, LLC",
        "drug_brand": "BENZOYL PEROXIDE"
    },
    {
        "latitude": "43.3128",
        "longitude": "-1.975",
        "drug_company": "Baxter Healthcare Corporation",
        "drug_brand": "Potassium Chloride in Sodium Chloride"
    },
    {
        "latitude": "22.81027",
        "longitude": "115.83058",
        "drug_company": "Hospira, Inc.",
        "drug_brand": "Nalbuphine Hydrochloride"
    },
    {
        "latitude": "28.33672",
        "longitude": "120.21782",
        "drug_company": "Ecolab Inc.",
        "drug_brand": "Quik-Care"
    },
    {
        "latitude": "21.40726",
        "longitude": "110.68453",
        "drug_company": "Wal-Mart Stores Inc",
        "drug_brand": "Equate Maximum Strength Spot Treatment"
    },
    {
        "latitude": "33.9339",
        "longitude": "112.89382",
        "drug_company": "REMEDYREPACK INC.",
        "drug_brand": "Isoniazid"
    },
    {
        "latitude": "41.1333",
        "longitude": "-7.3",
        "drug_company": "Unit Dose Services",
        "drug_brand": "Sertraline Hydrochloride"
    },
    {
        "latitude": "11.3595",
        "longitude": "10.6732",
        "drug_company": "STI Pharma LLC",
        "drug_brand": "SULFATRIM"
    },
    {
        "latitude": "43.4074",
        "longitude": "5.0553",
        "drug_company": "Shield Line LLC",
        "drug_brand": "Medpride"
    },
    {
        "latitude": "34.88333",
        "longitude": "136.98333",
        "drug_company": "Noveko Inc",
        "drug_brand": "Microban Unscented"
    },
    {
        "latitude": "49.5833",
        "longitude": "3",
        "drug_company": "Genentech, Inc.",
        "drug_brand": "Rocephin"
    },
    {
        "latitude": "-8.1294",
        "longitude": "113.6275",
        "drug_company": "Alaven Pharmaceutical LLC",
        "drug_brand": "Proctofoam"
    },
    {
        "latitude": "59.2833",
        "longitude": "18.3",
        "drug_company": "Have and Be Co., Ltd.",
        "drug_brand": "Dr.Jart Prescriptment All That Lift and Firm"
    },
    {
        "latitude": "5.71434",
        "longitude": "-72.93391",
        "drug_company": "REMEDYREPACK INC.",
        "drug_brand": "Metoprolol Tartrate"
    },
    {
        "latitude": "31.03735",
        "longitude": "114.53563",
        "drug_company": "Harris Pharmaceutical, Inc.",
        "drug_brand": "Mometasone Furoate"
    },
    {
        "latitude": "50.89713",
        "longitude": "19.2156",
        "drug_company": "GOJO Industries, Inc.",
        "drug_brand": "MICRELL Antibacterial Foam Handwash"
    },
    {
        "latitude": "13.88953",
        "longitude": "43.9688",
        "drug_company": "Dolgencorp Inc",
        "drug_brand": "DG Health Cold and Flu Relief"
    },
    {
        "latitude": "55.53028",
        "longitude": "58.25278",
        "drug_company": "A-S Medication Solutions LLC",
        "drug_brand": "Penicillin V Potassium"
    },
    {
        "latitude": "53.83734",
        "longitude": "16.43512",
        "drug_company": "Clinical Solutions Wholesale",
        "drug_brand": "Perphenazine"
    },
    {
        "latitude": "-6.692",
        "longitude": "111.4391",
        "drug_company": "Natureplex LLC",
        "drug_brand": "Acne Treatment"
    },
    {
        "latitude": "14.9265",
        "longitude": "120.55791",
        "drug_company": "Winning Solutions",
        "drug_brand": "Miracle Foot Repair"
    },
    {
        "latitude": "39.3213",
        "longitude": "-9.3143",
        "drug_company": "MedVantx, Inc.",
        "drug_brand": "clonidine hydrochloride"
    },
    {
        "latitude": "39.7",
        "longitude": "-8.9833",
        "drug_company": "Depomed, Inc.",
        "drug_brand": "Proquin"
    },
    {
        "latitude": "37.43333",
        "longitude": "140.48333",
        "drug_company": "AMOREPACIFIC",
        "drug_brand": "SULWHASOO EVENFAIR PERFECTING CUSHION NO.23"
    },
    {
        "latitude": "-6.9116",
        "longitude": "113.2323",
        "drug_company": "Personal Care Products",
        "drug_brand": "Vagi care"
    },
    {
        "latitude": "38.7",
        "longitude": "-7.7833",
        "drug_company": "H.J. Harkins Company, Inc.",
        "drug_brand": "misoprostol"
    },
    {
        "latitude": "23.08428",
        "longitude": "114.18279",
        "drug_company": "Baxter Healthcare Corporation",
        "drug_brand": "Osmitrol"
    },
    {
        "latitude": "60.1496",
        "longitude": "15.1878",
        "drug_company": "Nelco Laboratories, Inc.",
        "drug_brand": "Papaya"
    },
    {
        "latitude": "12.48039",
        "longitude": "108.0321",
        "drug_company": "Nelco Laboratories, Inc.",
        "drug_brand": "Cacao Bean"
    },
    {
        "latitude": "50.38453",
        "longitude": "23.43839",
        "drug_company": "Unit Dose Services",
        "drug_brand": "Alprazolam"
    },
    {
        "latitude": "48.8592",
        "longitude": "2.3417",
        "drug_company": "ALK-Abello, Inc.",
        "drug_brand": "CENTER-AL - AMBROSIA TRIFIDA POLLEN"
    },
    {
        "latitude": "50.60495",
        "longitude": "13.78748",
        "drug_company": "FDN Enterprises, LLC",
        "drug_brand": "Classic Care Apricot Scrub"
    },
    {
        "latitude": "-8.3052",
        "longitude": "123.237",
        "drug_company": "Bare Escentuals Beauty, Inc.",
        "drug_brand": "bareMinerals bareSkin Pure Brightening Serum Foundation Broad Spectrum SPF 20"
    },
    {
        "latitude": "9.69871",
        "longitude": "-68.43348",
        "drug_company": "MedVantx, Inc.",
        "drug_brand": "Sertraline Hydrochloride"
    },
    {
        "latitude": "-11.35",
        "longitude": "-69.58333",
        "drug_company": "Dentsply Pharmaceutical Inc.",
        "drug_brand": "Citanest Forte"
    },
    {
        "latitude": "-6.3497",
        "longitude": "106.0254",
        "drug_company": "Johnson \u0026 Johnson Healthcare Products, Division of McNEIL-PPC, Inc.",
        "drug_brand": "Mens Rogaine"
    },
    {
        "latitude": "21.72288",
        "longitude": "104.9113",
        "drug_company": "Uriel Pharmacy Inc.",
        "drug_brand": "Regio substantiae nigrae 6 Special Order"
    },
    {
        "latitude": "-2.90472",
        "longitude": "-41.77667",
        "drug_company": "CCA Industries, Inc.",
        "drug_brand": "SOLAR SENSE ZANY ZINC SPF 50 - GREEN"
    },
    {
        "latitude": "28.25",
        "longitude": "104.16667",
        "drug_company": "Antigen Laboratories, Inc.",
        "drug_brand": "English Plantain"
    },
    {
        "latitude": "55.6325",
        "longitude": "13.0714",
        "drug_company": "Nomax Inc.",
        "drug_brand": "GloStrips"
    },
    {
        "latitude": "37.94843",
        "longitude": "22.79203",
        "drug_company": "Akorn, Inc.",
        "drug_brand": "Cyclopentolate Hydrochloride"
    },
    {
        "latitude": "30.0659",
        "longitude": "120.35579",
        "drug_company": "Acella Pharmaceuticals, LLC",
        "drug_brand": "PNV-First"
    },
    {
        "latitude": "-10.2834",
        "longitude": "123.3177",
        "drug_company": "sanofi-aventis U.S. LLC",
        "drug_brand": "Primaquine Phosphate"
    },
    {
        "latitude": "44.21028",
        "longitude": "43.13528",
        "drug_company": "WIZCOZ CORPORATION LTD",
        "drug_brand": "SKIN79 Super Plus Beblesh Balm Triple Functions SPF25 PA (5g)"
    },
    {
        "latitude": "15.9583",
        "longitude": "120.40973",
        "drug_company": "EME Medical Equipment (Ephrata Medical Equip.)",
        "drug_brand": "Oxygen"
    },
    {
        "latitude": "19.32076",
        "longitude": "95.18272",
        "drug_company": "3M Health Care",
        "drug_brand": "Broad Spectrum SPF 30"
    },
    {
        "latitude": "30.2148",
        "longitude": "110.4488",
        "drug_company": "The Mentholatum Company",
        "drug_brand": "Softlips"
    },
    {
        "latitude": "49.2567",
        "longitude": "2.4848",
        "drug_company": "Sun Pharmaceutical Industries, Inc.",
        "drug_brand": "TRAMADOL HYDROCHLORIDE AND ACETAMINOPHEN"
    },
    {
        "latitude": "48.58876",
        "longitude": "95.43257",
        "drug_company": "ALK-Abello, Inc.",
        "drug_brand": "TRITICUM AESTIVUM POLLEN"
    },
    {
        "latitude": "38.02544",
        "longitude": "23.87617",
        "drug_company": "Torrent Pharmaceuticals Limited",
        "drug_brand": "Sertraline hydrochloride"
    },
    {
        "latitude": "-7.3367",
        "longitude": "108.1011",
        "drug_company": "SJ Creations, Inc.",
        "drug_brand": "Sweet Fig Antibacterial Foaming Hand Wash"
    },
    {
        "latitude": "-8.4569",
        "longitude": "120.5722",
        "drug_company": "Kroger Company",
        "drug_brand": "hemorrhoidal"
    },
    {
        "latitude": "41.55",
        "longitude": "-8.4333",
        "drug_company": "Physicians Formula Inc",
        "drug_brand": "Super CC Plus Cream"
    },
    {
        "latitude": "39.61361",
        "longitude": "117.05333",
        "drug_company": "KIK Custom Products",
        "drug_brand": "Banana Boat Sport Performance"
    },
    {
        "latitude": "28.51068",
        "longitude": "120.04901",
        "drug_company": "St Marys Medical Park Pharmacy",
        "drug_brand": "VERAPAMIL"
    },
    {
        "latitude": "10.65298",
        "longitude": "-61.36298",
        "drug_company": "Accra-Pac, Inc.",
        "drug_brand": "Banana Boat UltraMist Kids Tear Free Lotion SPF 50"
    },
    {
        "latitude": "-19.55",
        "longitude": "169.26667",
        "drug_company": "Takeda Pharmaceuticals America, Inc.",
        "drug_brand": "Nesina"
    },
    {
        "latitude": "29.28912",
        "longitude": "111.58562",
        "drug_company": "Procter \u0026 Gamble Manfuacturing Company",
        "drug_brand": "Crest Cavity Protection"
    },
    {
        "latitude": "58.4167",
        "longitude": "15.6167",
        "drug_company": "Physicians Total Care, Inc.",
        "drug_brand": "Hydrocodone Bitartrate And Acetaminophen"
    },
    {
        "latitude": "0.14411",
        "longitude": "100.1657",
        "drug_company": "STAT Rx USA LLC",
        "drug_brand": "Diclofenac Sodium"
    }
];